<?php return [
    'acceding' => 'Acceso al panel',
    'email' => 'Correo electrónico',
    'password' => 'Contraseña',
    'remember' => 'Recordarme',
    'forgot' => 'Ha olvidado su contraseña?',
    'login' => 'Acceder',
    'logout' => 'Salir',
    'register' => 'Registrarse',
    'name' => 'Nombre',
    'reset' => 'Reiniciar Contraseña',
    'send_reset' => 'Enviar enlace de reinicio de contraseña',
    'password_confirm' => 'Confirmar contraseña'
];