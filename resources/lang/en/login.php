<?php return [
    'acceding' => 'Panel access',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'remember' => ' Remember Me',
    'forgot' => 'Forgot Your Password?',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'name' => 'Name',
    'reset' => 'Reset Password',
    'send_reset' => 'Send Password Reset Link',
    'password_confirm' => 'Confirm Password'
];